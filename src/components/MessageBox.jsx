import React from 'react';
import trim from 'trim';
import Firebase from 'firebase';
import Card from 'material-ui/lib/card/card';

class MessageBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
    this.firebaseRef = new Firebase('https://react-stack-t.firebaseio.com/messages');

    this.onChange = this.onChange.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  onChange(e) {
    this.setState({
      message: e.target.value,
    });
  }

  onKeyUp(e) {
    const message = trim(e.target.value);
    if ((e.keyCode === 13) && (message !== '')) {
      e.preventDefault();
      this.firebaseRef.push({ message });
      this.clearMessage();
    }
  }

  clearMessage() {
    this.setState({ message: '' });
  }

  render() {
    return (
      <Card style={{
        maxWidth: 1200,
        margin: '30px auto',
        padding: '30px',
      }}
      >
        <textarea
          value={this.state.message}
          onChange={this.onChange}
          onKeyUp={this.onKeyUp}
          style={{
            width: '100%',
            borderColor: '#D0D0D0',
            resize: 'none',
            borderRadius: '3px',
            minHeight: '50px',
            color: '#555',
            fontSize: '14px',
            outline: 'none',
          }}
        />
      </Card>
    );
  }
}

export default MessageBox;
