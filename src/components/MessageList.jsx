import React from 'react';
import Firebase from 'firebase';
import _ from 'lodash';
import Message from './Message.jsx';
import Card from 'material-ui/lib/card/card';
import List from 'material-ui/lib/lists/list';

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: {},
    };

    this.firebaseRef = new Firebase('https://react-stack-t.firebaseio.com/messages');
    this.firebaseRef.on('child_added', this.onChildAdded.bind(this));
    this.firebaseRef.on('child_removed', this.onChildRemoved.bind(this));
  }

  onChildAdded(addedMsg) {
    const key = addedMsg.key();
    if (this.state.messages[key]) {
      return;
    }
    const message = addedMsg.val();
    this.state.messages[key] = message;
    this.setState({ messages: this.state.messages });
  }

  onChildRemoved(removedMsg) {
    const key = removedMsg.key();
    if (!this.state.messages[key]) {
      return;
    }
    delete this.state.messages[key];
    this.setState({ messages: this.state.messages });
  }

  render() {
    const messageNodes = _.values(this.state.messages).map((message) => {
      return (
        <Message message={message.message} />
      );
    });
    return (
      <Card style={{
        flexGrow: 2,
        marginLeft: '30px',
      }}
      >
        <List>
          {messageNodes}
        </List>
      </Card>
    );
  }
}

export default MessageList;
