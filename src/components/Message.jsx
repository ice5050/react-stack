import React from 'react';
import Avatar from 'material-ui/lib/avatar';
import ListItem from 'material-ui/lib/lists/list-item';

const propTypes = {
  message: React.PropTypes.string.isRequired,
};

class Message extends React.Component {
  render() {
    return (
      <ListItem
        leftAvatar={<Avatar src={`http://api.adorable.io/avatars/285/${Math.random().toString(36).substring(7)}`} />}
      >
        {this.props.message}
      </ListItem>
    );
  }
}

Message.propTypes = propTypes;

export default Message;
