import React from 'react';
import ListItem from 'material-ui/lib/lists/list-item';

const propTypes = {
  channel: React.PropTypes.string.isRequired,
};

class Channel extends React.Component {
  render() {
    return (
      <ListItem>{this.props.channel}</ListItem>
    );
  }
}

Channel.propTypes = propTypes;

export default Channel;
