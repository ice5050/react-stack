import Colors from 'material-ui/lib/styles/colors';

const theme = {
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: Colors.blue500,
    primary2Color: Colors.blue700,
    primary3Color: Colors.blue100,
    accent1Color: Colors.pink400,
  },
};

export default theme;
